Webpack最基本的配置模板
-----------------------

#### 初始化项目
```shell
mkdir hello-world
cd hello-world
npm init
```

> **npm init 步骤说明：**
> package name: 你的项目名字叫啥
version: 版本号
description: 对项目的描述
entry point: 项目的入口文件（一般你要用那个js文件作为node服务，就填写那个文件）
test command: 项目启动的时候要用什么命令来执行脚本文件（默认为node app.js）
git repository:  如果你要将项目上传到git中的话，那么就需要填写git的仓库地址
keywirds：项目关键字
author: 作者的名字
license: 发行项目需要的证书

### 安装必要依赖
##### 一、webpack及相关插件
###### 1.1 安装依赖
```shell
npm install -D webpack webpack-cli webpack-dev-server
```

在这里贴出我最终安装依赖的版本号，提供参考：
```json
"devDependencies": {
    "webpack": "^4.46.0",
    "webpack-cli": "^3.3.12",
    "webpack-dev-server": "^3.11.2"
}
```
> npm install --save-dev webpack@4
  npm install --save-dev webpack-cli@3
  npm install --save-dev  webpack-dev-server

###### 1.2 验证安装的webpack是否能正常工作：

1、src目录下创建main.js(入口文件)并写入：
```javascript
console.log("hello world");
```

2、根目录下新建`webpack.config.js`,并写入：
```javascript
const path = require("path");

module.exports = {
    mode: "production", // 开发模式，可选production，development
    entry: path.resolve(__dirname, "./src/main.js"), // 入口文件
    output: {
        path: path.resolve(__dirname, './dist'), // 打包生成的文件地址，必须是绝对路径
        filename: "[name].build.js" // 生成的文件名
    }
}
```

3、在`package.json`中的`scripts`中添加一个脚本：
```json
"scripts": {
    "build": "webpack"
}
```

4、在命令行中执行`npm run build`，或直接运行`webpack`,此时项目目录中出现了`dist/main.build.js`，证明执行成功

> **说明：**
> 1、webpack在执行时，默认会搜索当前目录下webpack.config.js, 或者通过--config选项来指定配置文件。
> 2、development模式和production模式：
**development 模式**:在开发环境中，我们能够更加方便的进行开发以及调试代码，当项目运行起来后，可以自动打开浏览器，webpack 能够通过监听代码的修改，自动重新编译打包，并且实时刷新浏览器。
**production模式**:在生产模式中，我们能够得到一个更小的 bundle，更轻量的 sourceMap，从而改善加载时间。如果没有设置mode，webpack默认以production模式进行打包。


##### 二、HTML相关依赖
js 文件打包成功后，需要一个 html 文件来引入这个 js 文件，这就需要用到我们安装`html-webpack-plugin`
```shell
npm install --save-dev html-webpack-plugin
```
> 与当前项目其他依赖版本对应 `npm install --save-dev  html-webpack-plugin@4`
> 该插件为html生成插件，它会将生成的js和css文件自动插入到html中。

新建`public/index.html`创建一个基础页面：
```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- 以上代码告诉IE浏览器，IE8/9及以后的版本都会以最高版本IE来渲染页面 -->
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <title>Hello World</title>
    </head>
    <body>
        <!-- 如果浏览器禁止加载js脚本 -->
        <noscript>
            <strong>
                We're sorry but this site doesn't work properly without JavaScript
                enabled. Please enable it to continue.
            </strong>
        </noscript>
        <div id="app"></div>
        <!-- build后的文件会在这之后自动引入 -->
    </body>
</html>
```
在`public目录`下随便放入一个图标`favicon.ico`，然后在`webpack.config.js`中配置插件
```javascript
const HtmlWebpackPlugin = require("html-webpack-plugin");
...
plugins: [
    new HtmlWebpackPlugin({
        filename: "index.html", // 生成的文件名
        template: path.resolve(__dirname, "./public/index.html"), // 模板html
        favicon: path.resolve(__dirname, "./public/favicon.ico") // 图标
    })
]
```
之后再次执行`npm run build`，dist目录下会生成`index.html`，`favicon.ico`，`main.build.js`三个文件，通过浏览器打开`index.html`，就可以发现控制台输出了`hello world`，页面图标也变成了自己设定的图标，通过编辑器打开`index.html`，我们会发现 webpack 帮助我们自动引入了`favicon.ico`和`main.build.js`：
```html
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Hello World</title>
    <link rel="icon" href="favicon.ico">
</head>

<body>
    <noscript>
        <strong>We're sorry but this site doesn't work properly without JavaScript enabled. Please enable it to continue.
        </strong>
    </noscript>
    <div id="app"></div>
    <script src="main.build.js"></script>
</body>

</html>
```
##### 三、开启热加载
webpack 热加载需要用到webpack-dev-server，在开始我们已经安装过了，在`build`文件夹下`webpack.config.dev.js`中配置：
```javascript
...
devServer: {
    compress: true, // 是否压缩
    hot: true, // 不写默认也带热更新的效果
    host: "127.0.0.1",
    port: "8080",
    open: true,
    stats: 'errors-warnings'
}
```
增加package.json脚本：
```json
"start": "webpack-dev-server"
```
执行npm run start，打包成功后会自动打开网页，并且当你修改`src/main.js`或`index.html`的内容的时候，浏览器会自动重新打包并刷新。